import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        todos: []
    },
    mutations: {
        createTodo(state, newTodo) {
            state.todos = [newTodo, ...this.state.todos]
        },
        todoIsDone(state, todo) {
            state.todos = state.todos.filter(item => {
                if (item.id === todo.id)  item.done = !item.done
                return item
            })
        }
    },
    actions: {},
    getters: {
        allTodos(state) {
            return state.todos;
        }
    }
})

export default store